﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using IniParser;
using IniParser.Model;

namespace Firefox_Launcher
{
    public partial class Form1 : Form
    {
        #region vars
        List<FFProfile> profiles = new List<FFProfile>();
        #endregion
        public Form1()
        {
            InitializeComponent();
            this.Text = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;

            // Check Firefox Application localtion
            if (!File.Exists(@"C:\Program Files\Mozilla Firefox\firefox.exe") && !File.Exists(Properties.Settings.Default.FirefoxExec))
            {
                invalidPath("exe");
            } else
            {
                Properties.Settings.Default.FirefoxExec = File.Exists(Properties.Settings.Default.FirefoxExec) ? Properties.Settings.Default.FirefoxExec : @"C:\Program Files\Mozilla Firefox\firefox.exe";
                Properties.Settings.Default.Save();
            }

            // Check default firefox location
            if (!Directory.Exists($"{Environment.GetEnvironmentVariable("AppData")}/Mozilla") && !Directory.Exists(Properties.Settings.Default.MozillaFolder))
            {
                invalidPath("appdata");

            }
            else {
                Properties.Settings.Default.MozillaFolder = Directory.Exists(Properties.Settings.Default.MozillaFolder) ? Properties.Settings.Default.MozillaFolder : $"{Environment.GetEnvironmentVariable("AppData")}/Mozilla";
                Properties.Settings.Default.Save();
            }

            // Check if Profiles folder exists
            string profilesINIPath = $"{Properties.Settings.Default.MozillaFolder}/Firefox/profiles.ini";
            if (!File.Exists(profilesINIPath))
            {
                MessageBox.Show("Error", "Cannot find profiles configuration.");
                return;
            }

            // Load profiles
            FileIniDataParser IniParser = new FileIniDataParser();
            try
            {
                IniData IniData = IniParser.ReadFile(profilesINIPath);
                if (IniData.Sections.Count == 0) {
                    MessageBox.Show("Error", "No profiles found.");
                    return;
                }
                foreach (SectionData section in IniData.Sections)
                {
                    if (!section.SectionName.Contains("Profile") || section.Keys["IsRelative"] != "1" ) continue;
                    string id = section.SectionName.Substring(7);
                    string name = section.Keys["Name"];
                    string path = section.Keys["Path"];
                    profiles.Add(new FFProfile(id, name, path));
                }
                createUI();
            }
            catch
            {
                MessageBox.Show("Error", "Cannot read profile configuration");
                return;
            }
        }

        void invalidPath(string option)
        {
            switch (option)
            {
                case "exe":
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Title = "Locate your firefox.exe";
                    ofd.InitialDirectory = Directory.Exists(@"C:\Program Files") ? "@C:/Program Files" : @"C:\";
                    ofd.Filter = "*.exe|*.exe";
                    ofd.FileName = "firefox.exe";

                    DialogResult dr = ofd.ShowDialog();
                    if (dr != DialogResult.OK)
                    {
                        MessageBox.Show("Error", "Invalid path");
                        return;
                    }

                    Properties.Settings.Default.FirefoxExec = ofd.FileName;
                    Properties.Settings.Default.Save();
                    break;
                case "appdata":
                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    fbd.Description = "Locate your Firefox installation directory where profiles are stored.";
                    fbd.ShowDialog();
                    if (fbd.SelectedPath == "" || fbd.SelectedPath == null)
                    {
                        MessageBox.Show("Error", "Invalid path");
                        return;
                    }
                    Properties.Settings.Default.MozillaFolder = fbd.SelectedPath;
                    Properties.Settings.Default.Save();
                    break;
                default:
                    break;
            }
        }

        void createUI()
        {
            foreach (FFProfile profile in profiles)
            {
                Panel profilePanel = new Panel();
                profilePanel.Size = new Size(150, 100);
                flowLayoutPanel1.Controls.Add(profilePanel);

                Label lbName = new Label();
                lbName.Text = profile.name;
                lbName.Location = new Point(profilePanel.Width / 2, 0);
                profilePanel.Controls.Add(lbName);

                Button btLaunch = new Button();
                btLaunch.Size = new Size(70, 30);
                btLaunch.Location = new Point(profilePanel.Width / 2, profilePanel.Height - 5 - btLaunch.Size.Height);
                btLaunch.Text = "Launch";
                btLaunch.Tag = profile;
                btLaunch.Click += new EventHandler(RunProfile);
                profilePanel.Controls.Add(btLaunch);
            }
        }

        void RunProfile(object sender, EventArgs e)
        {
            string profileName = ((sender as Control).Tag as FFProfile).name;
            //MessageBox.Show(Properties.Settings.Default.FirefoxExec);
            System.Diagnostics.Process.Start(Properties.Settings.Default.FirefoxExec, $"-p {profileName}");
            Environment.Exit(0);
        }

        internal class FFProfile
        {
            internal string id;
            internal string name;
            internal string path;

            public FFProfile(string id, string name, string path)
            {
                this.id = id;
                this.name = name;
                this.path = path;
            }

            internal string FullName { 
                get { return this.path.Split('/')[1]; }
            }
        }

        private void BTChangeExec_Click(object sender, EventArgs e)
        {
            invalidPath("exe");
        }
    }
}
